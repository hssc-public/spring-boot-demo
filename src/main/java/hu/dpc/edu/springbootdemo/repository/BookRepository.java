package hu.dpc.edu.springbootdemo.repository;

import hu.dpc.edu.springbootdemo.entity.Book;
import org.springframework.dao.EmptyResultDataAccessException;

import java.util.List;

public interface BookRepository {
  long add(Book book);

  Book findById(long id) throws EntityNotFoundException;

  List<Book> findAll();

  void update(Book book);

  List<Book> findAllByExample(Book example);
}
