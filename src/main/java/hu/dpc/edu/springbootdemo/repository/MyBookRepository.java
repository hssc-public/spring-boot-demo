package hu.dpc.edu.springbootdemo.repository;

import hu.dpc.edu.springbootdemo.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface MyBookRepository extends JpaRepository<Book,Long> {


    List<Book> findByAuthorOrderByTitle(String author);
    List<Book> findAll();

}
