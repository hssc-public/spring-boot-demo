package hu.dpc.edu.springbootdemo.helper.sql;

public class SQLPredicate {
  public final String whereClause;
  public final Object[] values;

  public SQLPredicate(String whereClause, Object[] values) {
    this.whereClause = whereClause;
    this.values = values;
  }
}
