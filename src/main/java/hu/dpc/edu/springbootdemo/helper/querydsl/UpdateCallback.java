package hu.dpc.edu.springbootdemo.helper.querydsl;

import com.querydsl.sql.dml.SQLInsertClause;
import com.querydsl.sql.dml.SQLUpdateClause;

import java.sql.SQLException;

public interface UpdateCallback<R> {
  R doWithUpdateClause(SQLUpdateClause updateClause) throws SQLException;
}
