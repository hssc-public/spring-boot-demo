package hu.dpc.edu.springbootdemo.helper.querydsl;

import com.querydsl.core.types.*;
import com.querydsl.core.types.dsl.BooleanExpression;
import hu.dpc.edu.springbootdemo.entity.Book;

public class BookPredicates {

  public static Predicate findByExample(QBooks books, Book example) {
    Predicate predicate = null;

    final BooleanExpressionBuilder builder = new BooleanExpressionBuilder();

    if (example.getId() != null) {
      builder.and(books.id.eq(example.getId()));
    }
    if (example.getAuthor() != null) {
      builder.and(books.author.like("%" + example.getAuthor() + "%"));
    }
    if (example.getTitle() != null) {
      builder.and(books.title.like("%" + example.getTitle() + "%"));
    }

    return builder.build();
  }


  static class BooleanAsPredicate implements Predicate {
    public static Predicate TRUE = new BooleanAsPredicate(true);
    public static Predicate FALSE = new BooleanAsPredicate(false);
    private Expression<Boolean> constant;
    private boolean value;

    public BooleanAsPredicate(boolean value) {
      this.value = value;
      this.constant = ConstantImpl.create(value);
    }

    @Override
    public Class<? extends Boolean> getType() {
      return constant.getType();
    }

    @Override
    public <R, C> R accept(Visitor<R, C> v, C context) {
      return constant.accept(v, context);
    }

    @Override
    public Predicate not() {
      return this.value ? FALSE : TRUE;
    }
  }

  static class BooleanExpressionBuilder {
    private BooleanExpression currentExpresssion;

    public BooleanExpressionBuilder and(BooleanExpression booleanExpression) {
      if (currentExpresssion == null) {
        currentExpresssion = booleanExpression;
      } else {
        currentExpresssion = currentExpresssion.and(booleanExpression);
      }
      return this;
    }

    public Predicate build() {
      return currentExpresssion == null
              ? BooleanAsPredicate.TRUE
              : currentExpresssion;
    }
  }
}
