package hu.dpc.edu.springbootdemo.helper.querydsl;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;

import com.querydsl.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QBooks is a Querydsl query type for QBooks
 */
@Generated("com.querydsl.sql.codegen.MetaDataSerializer")
public class QBooks extends com.querydsl.sql.RelationalPathBase<QBooks> {

    private static final long serialVersionUID = -1289421546;

    public static final QBooks books = new QBooks("BOOKS");

    public final StringPath author = createString("author");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath title = createString("title");

    public final com.querydsl.sql.PrimaryKey<QBooks> constraint3 = createPrimaryKey(id);

    public QBooks(String variable) {
        super(QBooks.class, forVariable(variable), "PUBLIC", "BOOKS");
        addMetadata();
    }

    public QBooks(String variable, String schema, String table) {
        super(QBooks.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QBooks(String variable, String schema) {
        super(QBooks.class, forVariable(variable), schema, "BOOKS");
        addMetadata();
    }

    public QBooks(Path<? extends QBooks> path) {
        super(path.getType(), path.getMetadata(), "PUBLIC", "BOOKS");
        addMetadata();
    }

    public QBooks(PathMetadata metadata) {
        super(QBooks.class, metadata, "PUBLIC", "BOOKS");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(author, ColumnMetadata.named("AUTHOR").withIndex(3).ofType(Types.VARCHAR).withSize(100).notNull());
        addMetadata(id, ColumnMetadata.named("ID").withIndex(1).ofType(Types.DECIMAL).withSize(10).notNull());
        addMetadata(title, ColumnMetadata.named("TITLE").withIndex(2).ofType(Types.VARCHAR).withSize(100).notNull());
    }

}

