package hu.dpc.edu.springbootdemo.helper.querydsl;

import com.querydsl.sql.SQLQuery;

import java.sql.SQLException;

public interface QueryCallback<R> {
  R doWithQuery(SQLQuery<R> query) throws SQLException;
}
