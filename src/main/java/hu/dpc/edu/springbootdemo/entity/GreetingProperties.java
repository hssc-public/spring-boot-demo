package hu.dpc.edu.springbootdemo.entity;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("greeting")
public class GreetingProperties {
  private String template;
  private String defaultName;

  public String getTemplate() {
    return template;
  }

  public void setTemplate(String template) {
    this.template = template;
  }

  public String getDefaultName() {
    return defaultName;
  }

  public void setDefaultName(String defaultName) {
    this.defaultName = defaultName;
  }

  public String renderGreetingMessage(String name) {
    return String.format(template, name == null ? defaultName : name);
  }
}
