package hu.dpc.edu.springbootdemo.entity;

public enum Color {
  RED,
  GREEN,
  BLUE
}
