package hu.dpc.edu.springbootdemo;

import com.querydsl.sql.H2Templates;
import com.querydsl.sql.SQLTemplates;
import hu.dpc.edu.springbootdemo.entity.GreetingProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;

@SpringBootApplication
@EnableConfigurationProperties(GreetingProperties.class)
public class SpringBootDemoApplication {

	@Bean
	public SQLTemplates dialect() {
		return new H2Templates();
	}

	@Autowired
	private JdbcTemplate jdbcTemplate;

	public static void main(String[] args) {
		SpringApplication.run(SpringBootDemoApplication.class, args);
	}
}
