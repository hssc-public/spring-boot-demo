package hu.dpc.edu.springbootdemo.controller;

import hu.dpc.edu.springbootdemo.entity.GreetingProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingController {

  @Autowired
  private GreetingProperties properties;

  @GetMapping("/greet")
  public String greet(@RequestParam(required = false) String name) {
    return properties.renderGreetingMessage(name);
  }

}
