package hu.dpc.edu.springbootdemo.controller;

import hu.dpc.edu.springbootdemo.entity.Book;
import hu.dpc.edu.springbootdemo.repository.BookRepository;
import hu.dpc.edu.springbootdemo.repository.EntityNotFoundException;
import hu.dpc.edu.springbootdemo.repository.MyBookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.ExampleMatcher.GenericPropertyMatcher;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.List;

@RestController
@RequestMapping("/books")
public class BookController {

  @Autowired
  private MyBookRepository repository;


  @GetMapping()
  public List<Book> findByExample(@RequestParam(required = false) Long id,
                                  @RequestParam(required = false) String title,
                                  @RequestParam(required = false) String author) {

      final ExampleMatcher matcher = ExampleMatcher.matching()
              .withMatcher("id", GenericPropertyMatcher::exact)
              .withMatcher("title", GenericPropertyMatcher::contains)
              .withMatcher("author", GenericPropertyMatcher::contains);

      final Example<Book> example = Example.of(new Book(id,title, author), matcher);

    return repository.findAll(example);
  }

  @GetMapping("{id}")
  public Book findById(@PathVariable long id) {
    return repository.findById(id)
            .orElseThrow(() -> new EntityNotFoundException("Book not found with id " + id));
  }

  @PostMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public Book add(@RequestBody Book book) {
    book.setId(null); // ignoring id sent in
    repository.save(book);
    return book;
  }

  @PutMapping(path = "{id}", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public Book update(@PathVariable ("id")long id, @RequestBody Book book) {
    book.setId(id); // the id in the path determines the book Id, the id in the body is ignored.
    repository.findById(id).orElseThrow(() -> new EntityNotFoundException("Book with id " + id + " is not found."));
    repository.save(book);
    return book;
  }

  @ExceptionHandler(EntityNotFoundException.class)
  public ResponseEntity<String> handleEntityNotFoundException(EntityNotFoundException ex) {
    return ResponseEntity.status(HttpStatus.NOT_FOUND)
            .contentType(MediaType.TEXT_PLAIN)
            .body(ex.getMessage());
  }

}
