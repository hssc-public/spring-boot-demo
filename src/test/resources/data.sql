TRUNCATE TABLE BOOKS;

INSERT INTO BOOKS (ID, TITLE, AUTHOR) VALUES (1001, 'The Martian', 'Andy Weir');
INSERT INTO BOOKS (ID, TITLE, AUTHOR) VALUES (1002, 'The Catcher in the Rye', 'J. D. Salinger');
INSERT INTO BOOKS (ID, TITLE, AUTHOR) VALUES (1010, 'Harry Potter and the Philosopher''s Stone', 'J. K. Rowling');
