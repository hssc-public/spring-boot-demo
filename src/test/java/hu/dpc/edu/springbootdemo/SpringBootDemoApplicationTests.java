package hu.dpc.edu.springbootdemo;

import hu.dpc.edu.springbootdemo.entity.Book;
import hu.dpc.edu.springbootdemo.repository.BookRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class SpringBootDemoApplicationTests {

	@Autowired
	BookRepository repository;
	@Autowired
	JdbcTemplate jdbcTemplate;

	@Test
	public void hasThree() {
		final List<Book> response = repository.findAll();
		assertThat(response).hasSize(3);
	}

}
